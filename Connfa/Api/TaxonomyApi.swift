//
//  TaxonomyApi.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

class TaxonomyApi {
 
    //MARK:- urlSession methods
    func fetchTracks(completion: @escaping (Tracks?) -> Void){
        let url = getUrl(endPoint: .tracks)!
        let task = URLSession.shared.tracksTask(with: url) { (tracks, response, error) in
            guard error == nil else{
                completion(nil)
                return
            }
            completion(tracks)
        }
        task.resume()
    }
    
    func fetchLevels(completion: @escaping (Levels?) -> Void){
        let url = getUrl(endPoint: .levels)!
        let task = URLSession.shared.levelsTask(with: url) { (levels, response, error) in
            guard error == nil else{
                completion(nil)
                return
            }
            completion(levels)
        }
        task.resume()
    }
    
    func fetchTypes(completion: @escaping (Types?) -> Void){
        let url = getUrl(endPoint: .types)!
        let task = URLSession.shared.typesTask(with: url) { (types, response, error) in
            guard error == nil else{
                completion(nil)
                return
            }
            completion(types)
        }
        task.resume()
    }
    
    //MARK:- Private methods
    private func getUrl(endPoint: EndPoint) -> URL?{
        return ApiManager.shared.url(for: endPoint)
    }
    
    
    //MARK:- get specific Taxonomy methods
    func getTrack(with id: Int, completion: @escaping (Track?) -> Void) {
        fetchTracks { (trackData) in
            let track = trackData?.tracks.filter{
                return $0.id == id
            }.first
            completion(track)
        }
    }
    
    func getType(with id: Int, completion: @escaping (Type?) -> Void) {
        fetchTypes { (typesData) in
            let type = typesData?.types.filter{
                return $0.id == id
            }.first
            completion(type)
        }
    }
    
    func getLevel(with id: Int, completion: @escaping (Level?) -> Void) {
        fetchLevels { (levelsData) in
            let level = levelsData?.levels.filter{
                return $0.id == id
                }.first
            completion(level)
        }
    }
}
