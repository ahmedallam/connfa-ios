//
//  ApiManager.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/20/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

class ApiManager {
    static var shared = ApiManager()
    
    private init(){}
    
    private let BASE_URL = "http://34.244.195.174/api/v2"
    
    private func conferenceUrl(with name: String) -> URL{
        var url = URL(string: BASE_URL)
        url?.appendPathComponent(name)
        return url!
    }
    
    func url(for endPoint: EndPoint) -> URL{
        var conferenceUrl = self.conferenceUrl(with: Constants.conferenceName)
        conferenceUrl.appendPathComponent(endPoint.rawValue)
        //append random query to prevent local cashe
        var urlComponent = URLComponents(url: conferenceUrl, resolvingAgainstBaseURL: false)
        let randomQuery = URLQueryItem(name: "", value: UUID().uuidString)
        urlComponent?.queryItems = [randomQuery]
        guard let url = urlComponent?.url else {
            fatalError("Error building url")
        }
        return url
    }
    
    //MARK:- APi Methods
    
    func getSessions(completion: @escaping (Sessions?) -> Void){
        let url = ApiManager.shared.url(for: .sessions)
        let task = URLSession.shared.sessionsTask(with: url) {
            (sessions, response, error) in
            guard error == nil else{
                completion(nil)
                return
            }
            completion(sessions)
        }
        task.resume()
    }
    
    func getSpeakers(completion: @escaping (_ speakers: [Speaker]?, _ error: Error?)->Void){
        let url = ApiManager.shared.url(for: .speakers)
        let task = URLSession.shared.speakersTask(with: url) { (speakersData, response, error) in
            guard error == nil else{
                completion(nil, error)
                return
            }
            completion(speakersData?.speakers, nil)
        }
        task.resume()
    }
    
    func getInfo(completion: @escaping (_ info: InfoData?) -> Void){
        let url = ApiManager.shared.url(for: .info)
        let task = URLSession.shared.infoTask(with: url) { (infoData, response, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
            completion(infoData)
        }
        task.resume()
    }
    
    func getLocations(completion: @escaping (_ locations: [Location]?) -> Void){
        let url = self.url(for: .locations)
        let task = URLSession.shared.locationsTask(with: url) { (allLocations, response, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
            completion(allLocations?.locations)
        }
        task.resume()
    }
    
    func getFloorPlans(completion: @escaping (_ locations: [FloorPlan]?) -> Void){
        let url = self.url(for: .floorPlans)
        let task = URLSession.shared.floorPlansTask(with: url) { (floorPlans, response, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
            completion(floorPlans?.floorPlans)
        }
        task.resume()
    }
    
    
}


enum EndPoint: String, Codable{
    case speakers = "getSpeakers"
    case sessions = "getSessions"
    case tracks = "getTracks"
    case levels = "getLevels"
    case types = "getTypes"
    case info = "getInfo"
    case locations = "getLocations"
    case floorPlans = "getFloorPlans"
}
