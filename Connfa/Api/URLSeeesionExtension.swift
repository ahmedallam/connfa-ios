//
//  URLSeeesionExtension.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/20/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            do{
                let p = try JSONDecoder().decode(T.self, from: data)
                print(p)
            }catch let e{
                print(e)
            }
            completionHandler(try? JSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func speakersTask(with url: URL, completionHandler: @escaping (Speakers?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func sessionsTask(with url: URL, completionHandler: @escaping (Sessions?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func levelsTask(with url: URL, completionHandler: @escaping (Levels?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func tracksTask(with url: URL, completionHandler: @escaping (Tracks?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func typesTask(with url: URL, completionHandler: @escaping (Types?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func infoTask(with url: URL, completionHandler: @escaping (InfoData?, URLResponse?, Error?) -> Void) -> URLSessionDataTask{
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func locationsTask(with url: URL, completionHandler: @escaping (Locations?, URLResponse?, Error?) -> Void) -> URLSessionDataTask{
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func floorPlansTask(with url: URL, completionHandler: @escaping (FloorPlans?, URLResponse?, Error?) -> Void) -> URLSessionDataTask{
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
}
