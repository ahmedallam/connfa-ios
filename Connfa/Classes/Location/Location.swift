//
//  Location.swift
//  Connfa
//
//  Created by Ahmed Allam on 3/3/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation
import CoreLocation

struct Locations: Codable {
    var locations: [Location]
}

struct Location: Codable {
    var id: Int!
    var address: String!
    var name: String!
    var imageUrl: String?
    var latitude: String!
    var longitude: String!
    var order: Int
    var deleted: Bool
    
    enum CodingKeys: String, CodingKey{
        case id = "locationId"
        case name = "locationName"
        case address, latitude, longitude, order, deleted
    }
    
    var clLocation: CLLocation {
        return CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!)
    }
    
    var clLocationCoordinate2D: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
    }
    
    var streetAndNumber: String {
        let components = address.components(separatedBy: ",")
        return components.first ?? ""
    }
    var cityAndProvince: String {
        let components = address.components(separatedBy: ",")
        return components.count > 1 ? components[1] : ""
    }
    var state: String {
        let components = address.components(separatedBy: ",")
        return components.count > 2 ? components[2] : ""
    }
}
