//
//  Speakers.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/19/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct Speakers: Codable {
    var speakers: [Speaker]
}

struct Speaker: Codable{
    
    enum CodingKeys: String, CodingKey {
        
        case id = "speakerId"
        case avatarUrl = "avatarImageURL"
        case organisationName = "organizationName"
        case firstName
        case lastName
        case jobTitle
        case characteristic
        case twitterName
        case webSite
        case email
    }
    
    
    var id: Int!
    var avatarUrl: String?
    var firstName: String!
    var lastName: String!
    var organisationName: String?
    var jobTitle: String?
    var characteristic: String?
    var eventsUniqueNames: [String] = []
    var twitterName: String?
    var webSite: String?
    var email: String?

    var nameHeader: String {
        return String(firstName[firstName.startIndex]).uppercased()
    }
    
    var fullName: String {
        return firstName + " " + lastName
    }
    
    var uniqueName: String{
        return fullName + "_" + String(id)
    }
    
    var abbreviation: String {
        var result: String = String()
        if firstName.count > 0 {
            result.append(nameHeader)
        }
        if lastName.count > 0 {
            result.append(lastName[lastName.startIndex])
        }
        return result == "" ? "Unknown" : result.uppercased()
    }
    
    var organisationAndPosition: String {
        return [organisationName, jobTitle].compactMap{ $0 }.joined(separator: " / ")
    }
    
}
