//
//  SpeakersStore.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

class SpeakersStore {
    static var shared = SpeakersStore()
    var speakers: [Speaker] = []
    private init(){}
    
    //Add event unique name to speaker while fetching events
    func add(eventUniqueName name: String, to speakerIds: [Int]){
        for id in speakerIds{
            if let index = getSpeakerIndex(with: id){
                speakers[index].eventsUniqueNames.append(name)
            }
        }
    }
    
    func getSpeakerIndex(with id: Int) -> Int?{
        return speakers.firstIndex {$0.id == id}
    }
    
    func getSpeaker(with id: Int) -> Speaker?{
        return speakers.filter{ $0.id == id }.first
    }
    
    public func speakersFrom(eventUniqueName: String) -> [Speaker] {
       return self.speakers.filter{ $0.eventsUniqueNames.contains(eventUniqueName) }
    }
    
    public func speakerBy(uniqueName: String) -> Speaker? {
        return speakers.filter{ $0.uniqueName == uniqueName }.first
    }
    
}
