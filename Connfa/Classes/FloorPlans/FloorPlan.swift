//
//  FloorPlan.swift
//  Connfa
//
//  Created by Ahmed Allam on 3/3/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct FloorPlans: Codable {
    var floorPlans: [FloorPlan]
}

struct FloorPlan: Codable {
    var id: Int!
    var floorPlanName: String!
    var floorPlanImageURL: String!
    var order: Int?
    var deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "floorPlanId"
        case floorPlanName, floorPlanImageURL, order, deleted
    }
}
