//
//  InfoModel.swift
//  Connfa
//
//  Created by Ahmed Allam on 3/3/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct InfoData: Codable {
    var title: Title
    var info: [Info]
}


struct Title: Codable {
    var titleMajor: String
    var titleMinor: String
}


struct Info: Codable{
    var id: Int
    var infoTitle: String
    var html: String
    var order: Int
    var deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "infoId"
        case infoTitle
        case html
        case order
        case deleted
    }
}

