//
//  Tracks.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation
struct Tracks: Codable {
    let tracks: [Track]
}
struct Track: Codable {
    let id: Int
    let trackName: String
    let order: Int
    let deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "trackId"
        case trackName, order, deleted
    }
}
