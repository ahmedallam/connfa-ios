//
//  Levels.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct Levels: Codable {
    let levels: [Level]
}

struct Level: Codable {
    let id: Int
    let levelName: String
    let order: Int
    let deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "levelId"
        case levelName, order, deleted
    }
}
