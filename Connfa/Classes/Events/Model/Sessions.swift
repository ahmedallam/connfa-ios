//
//  Sessions.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

//typealias Sessions = [Session]

struct Sessions: Codable {
    let endPoint = EndPoint.sessions
    let days: [Session]
}

struct Session: Codable {
    let date: String
    let events: [Event]
}

struct Event: Codable {
    let id: Int
    let text, name, place: String
    let version: String?
    let experienceLevel, type: Int
    let from, to: String
    let speakers: [Int]
    let track, order: Int
    let link: String
    let deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "eventId"
        case text, name, place, version, experienceLevel, type, from, to, speakers, track, order, link, deleted
    }
}
