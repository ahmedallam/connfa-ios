//
//  Type.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct Types: Codable {
    let types: [Type]
}

struct Type: Codable {
    let id: Int
    let typeName: String
    let typeIconURL: String?
    let order: Int
    let deleted: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "typeId"
        case typeName, typeIconURL, order, deleted
    }
}
