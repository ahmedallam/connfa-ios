//
//  EventMapper.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

struct EventMapper {
    var taxonomiApi: TaxonomyApi!
    
    init() {
        taxonomiApi = TaxonomyApi()
    }
    
    func mapEvent(fetchedEvent event: Event) -> EventModel{
        let eventModel = EventModel()
        let formatter = defaultFormatter
        
        eventModel.id = "\(event.id)"
        eventModel.uniqueName = "\(event.name)_\(event.id)"
        eventModel.name = event.name
        eventModel.from = event.from
        eventModel.to = event.to
        eventModel.text = event.text
        eventModel.place = event.place
        // minus 1 as local database satrted from 1 and app started from 0
        eventModel.experienceLevel = event.experienceLevel - 1
        eventModel.link = event.link
        eventModel.isSelectable = true
        
        eventModel.fromDate = formatter.date(from: event.from)
        eventModel.toDate = formatter.date(from: event.to)
        
        taxonomiApi.getType(with: event.type, completion: { (type) in
            eventModel.type = type?.typeName ?? ""
        })
        
        taxonomiApi.getTrack(with: event.track) { (track) in
            eventModel.track = track?.trackName ?? ""
        }
        
        eventModel.speakers = getSpeakersTuples(from: event.speakers)
        SpeakersStore.shared.add(eventUniqueName: eventModel.uniqueName, to: event.speakers)
        return eventModel
    }
    
    func getSpeakersTuples(from ids: [Int]) -> [EventModel.Speakers]{
        var speakers: [EventModel.Speakers] = []
        for id in ids{
            if let speaker = SpeakersStore.shared.getSpeaker(with: id){
                let speakerTuple = (speakerUniqueName: speaker.uniqueName, speakerFullName: speaker.fullName)
                speakers.append(speakerTuple)
            }
        }
        return speakers
    }
    
}
