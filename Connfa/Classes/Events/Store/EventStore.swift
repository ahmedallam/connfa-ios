//
//  EventStore.swift
//  Connfa
//
//  Created by Ahmed Allam on 2/24/19.
//  Copyright © 2019 Lemberg Solution. All rights reserved.
//

import Foundation

class EventStore {
    
    private let taxonomyApi: TaxonomyApi!
    private let eventMapper: EventMapper!
    let group = DispatchGroup()
    
    init() {
        taxonomyApi = TaxonomyApi()
        eventMapper = EventMapper()
    }
    
    func fetchEvents(completion: @escaping ([EventModel]) -> Void){
        
        ApiManager.shared.getSessions { [weak self] sessions in
            var events: [EventModel] = []
            guard let sessions = sessions else {return}
            for day in sessions.days{
                for event in day.events{
                    self?.group.enter()
                    if let mappedEvent = self?.eventMapper.mapEvent(fetchedEvent: event){
                        self?.taxonomyApi.getType(with: event.type, completion: { (type) in
                            mappedEvent.type = type?.typeName ?? ""
                        })
                        
                        self?.taxonomyApi.getTrack(with: event.track) { (track) in
                            mappedEvent.track = track?.trackName ?? ""
                            self?.group.leave()
                        }
                        events.append(mappedEvent)
                       
                    }
                }
            }
            self?.group.notify(queue: .main){
                completion(events)
            }
        }
    }
    
    
}
