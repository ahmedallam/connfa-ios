//
//  EventModel.swift
//  Connfa
//
//  Created by Marian Fedyk on 12/14/17.
//  Copyright © 2017 Lemberg Solution. All rights reserved.
//

import Foundation
import EventKit

class EventModel{
    
    typealias Speakers = (speakerUniqueName: String, speakerFullName: String)
    
    static var keyPath: String {
        return Keys.events
    }
    
    static var notification: Notification.Name? {
        return .receiveEvent
    }
    
    var speakersJoinedFullNames: String? {
        return speakers.count > 0 ? (speakers.map{ $0.speakerFullName }).joined(separator: ", ") : nil
    }
    
    func convertedToCalendarEvent(eventStore: EKEventStore) -> EKEvent {
        let event = EKEvent(eventStore: eventStore)
        event.title = name
        event.startDate = fromDate
        event.endDate = toDate
        return event
    }
    
    var id: String!
    var uniqueName: String!
    var name: String!
    var from: String!
    var to: String!
    var text: String?
    var place: String?
    var experienceLevel: Int!
    var type: String?
    var track: String?
    var link: String?
    var fromDate: Date!
    var toDate: Date!
    var isSelectable: Bool = true
    var speakers: [Speakers] = []
}
