A new Connfa (Swift) is in master, an old Objective-C project now is available in branch [connfa-objc](https://github.com/lemberg/connfa-ios/tree/connfa-objc) 

Documentation is available on [http://connfa.com/](http://connfa.com/)

Project is supported by [Lemberg Solutions](http://lemberg.co.uk)
